package com.certif.kata;

import java.util.List;

public class Kata {

  public static final int ONE = 1;
  private static final int FIVE = 5;
  private static final int TEN = 10;
  public static final int FIFTY = 50;
  public static final int ONE_HUNDRED = 100;
  public static final int FIVE_HUNDRED = 500;
  public static final int ONE_THOUSAND = 1000;
  public static final int ROMAN_LIMIT = 1001;

  private final List<Integer> boundaries = List.of(
          ONE,
          FIVE,
          TEN,
          FIFTY,
          ONE_HUNDRED,
          FIVE_HUNDRED,
          ONE_THOUSAND,
          ROMAN_LIMIT
  );

  public String convert(int decimalNumber) {
    int decimalRemaining = decimalNumber;
    String romanNumber = "";

    while (decimalRemaining > 0) {
      int lowerBoundary;
      int biggestDecimal = getBiggestDecimal(decimalRemaining);

      String prefixedRomanNumber = getPrefixedRomanNumber(biggestDecimal);

      romanNumber += prefixedRomanNumber;
      if (!prefixedRomanNumber.isEmpty()) {
        decimalRemaining -= biggestDecimal;
        continue;
      }

      try {
        lowerBoundary = getLowerBoundaryOf(decimalRemaining);
      } catch (Exception e) {
        return "Number is not convertible";
      }

      decimalRemaining -= lowerBoundary;
      romanNumber += convertToOneRomanNumber(lowerBoundary);
    }

    return romanNumber;
  }

  private String getPrefixedRomanNumber(int biggestDecimal) {
    if (biggestDecimal + ONE == FIVE || biggestDecimal + ONE == TEN) {
      return convertToOneRomanNumber(ONE) + convertToOneRomanNumber(biggestDecimal + ONE);
    } else if (biggestDecimal + TEN == FIFTY || biggestDecimal + TEN == ONE_HUNDRED) {
      return convertToOneRomanNumber(TEN) + convertToOneRomanNumber(biggestDecimal + TEN);
    } else if (biggestDecimal + ONE_HUNDRED == FIVE_HUNDRED || biggestDecimal + ONE_HUNDRED == ONE_THOUSAND) {
      return convertToOneRomanNumber(ONE_HUNDRED) + convertToOneRomanNumber(biggestDecimal + ONE_HUNDRED);
    } else {
      return "";
    }
  }

  private int getBiggestDecimal(int decimalRemaining) {
    int biggestDecimal = 1;
    int powerOfTen = 1;
    for (int i = 0; i <= 3; i++) {
      if (decimalRemaining / powerOfTen > 0 && decimalRemaining / (powerOfTen * 10) == 0) {
        biggestDecimal = (decimalRemaining / powerOfTen) * powerOfTen;
      }
      powerOfTen *= 10;
    }
    return biggestDecimal;
  }

  private int getLowerBoundaryOf(int decimalRemaining) throws Exception {
    for (int i = 0; i < boundaries.size() - 1; i++) {
      int firstBoundary = boundaries.get(i), secondBoundary = boundaries.get(i + 1);

      if (isInBoundary(decimalRemaining, firstBoundary, secondBoundary)) {
        return firstBoundary;
      }
    }

    throw new Exception("Could not find this number's boundary");
  }

  private boolean isInBoundary(int decimalRemaining, int firstBoundary, int secondBoundary) {
    return decimalRemaining >= firstBoundary && decimalRemaining < secondBoundary;
  }

  private String convertToOneRomanNumber(int number) {
    if (number == FIVE)
      return "V";
    if (number == TEN)
      return "X";
    if (number == FIFTY)
      return "L";
    if (number == ONE_HUNDRED)
      return "C";
    if (number == FIVE_HUNDRED)
      return "D";
    if (number == ONE_THOUSAND)
      return "M";
    return "I";
  }
}
