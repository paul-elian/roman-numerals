package com.certif.kata;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class KataTest {

    private final Kata kata = new Kata();

    @Test
    @DisplayName("When given a number, it should return one roman letter")
    void whenGivenANumberItShouldReturnARomanLetter() {
        assertEquals("I", kata.convert(1));
        assertEquals("V", kata.convert(5));
        assertEquals("X", kata.convert(10));
        assertEquals("L", kata.convert(50));
        assertEquals("C", kata.convert(100));
        assertEquals("D", kata.convert(500));
        assertEquals("M", kata.convert(1000));
    }

    @Test
    @DisplayName("When given a number, it should return multiple three roman letter")
    void whenGivenANumberItShouldReturnThreeRomanLetter() {
        assertEquals("LI", kata.convert(51));
        assertEquals("II", kata.convert(2));
        assertEquals("III", kata.convert(3));
        assertEquals("CI", kata.convert(101));
        assertEquals("XVI", kata.convert(16));
        assertEquals("CLI", kata.convert(151));
        assertEquals("DCCCLVI", kata.convert(856));
    }

    @Test
    @DisplayName("When given a number, it should return more than three roman letter")
    void whenGivenANumberItShouldReturnMoreThanThreeRomanLetter() {
        assertEquals("IV", kata.convert(4));
        assertEquals("IX", kata.convert(9));
        assertEquals("CIX", kata.convert(109));
        assertEquals("XLI", kata.convert(41));
        assertEquals("CDXLIX", kata.convert(449));
        assertEquals("XL", kata.convert(40));
        assertEquals("XC", kata.convert(90));
        assertEquals("CD", kata.convert(400));
        assertEquals("CM", kata.convert(900));
    }

    @Test
    @DisplayName("When number is greater than 1000 (excluded), it should not be convertible")
    void whenNumberIsGreaterThan1000ItShouldNotBeConvertible() {
        assertEquals("Number is not convertible", kata.convert(1001));
    }
}
